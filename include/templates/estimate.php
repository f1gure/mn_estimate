<?
$path = str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__);
$timeMultiplier = 1.25; // Коэффициент увеличения сроков после перевода в дни
$hourPerDay = 8; // Часов в рабочем дне
$vatPercent = 20;
?>
<img src="/<?=$path?>/../../images/logo.jpg" alt="" />
<br />
<p style="text-align:center; color: rgb(148,54,52); font-weight: bold;">
    <?=$arParams['item']['name']?>
    <?/*Смета на разработку сайта <?=$arParams['item']['domain']?> на системе управления Битрикс (БУС)*/?>
</p>
<br />
<table border="1" cellspacing="0" cellpadding="4">
    <tr>
        <td width="18.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Наименование</td>
        <td width="10.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Специалист</td>
        <td width="10.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Ставка<br />(внутр)</td>
        <td width="6%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Кол-во<br />(внутр)</td>
        <td width="12.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Стоимость<br />(внутр)</td>
        <td width="10.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Ставка</td>
        <td width="6%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Кол-во</td>
        <td width="12.5%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Стоимость</td>
        <td width="13%" bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" align="center" valign="middle">Маржа</td>
    </tr><?

    foreach ($arParams['services'] as $key => &$arService):?>
        <tr>
            <td bgcolor="#BFBFBF" style="font-size: 10pt; font-weight: bold;" colspan="9"><?=($key+1)?> этап: <?=($arService['title'] ?: $arService['name'])?></td>
        </tr><?
        $arService['total_ex'] = 0;
        $arService['time'] = 0;
        foreach ($arParams['item']['positions'][(int) $arService['id']] as $arPosition):?>
            <tr>
                <td style="font-size: 10pt;" valign="middle"><?=$arPosition['name']?></td>
                <td style="font-size: 10pt;" valign="middle"><?=$arPosition['title']?></td>
                <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arPosition['price'], 0, ',', ' ')?>,00 руб.</td>
                <td style="font-size: 10pt;" valign="middle" align="center"><?=$arPosition['count']?> <?=$arPosition['unit']?></td>
                <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arPosition['total'], 0, ',', ' ')?>,00 руб.</td>
                <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arPosition['price_ex'], 0, ',', ' ')?>,00 руб.</td>
                <td style="font-size: 10pt;" valign="middle" align="center"><?=$arPosition['count_ex']?> <?=$arPosition['unit']?></td>
                <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arPosition['total_ex'], 0, ',', ' ')?>,00 руб.</td>
                <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arPosition['total_ex'] - $arPosition['total'], 0, ',', ' ')?>,00 руб.</td>
            </tr><?

            $arService['total_ex'] += $arPosition['total_ex'];
            $arService['time'] += $arPosition['count_ex'] * $arPosition['count_ratio'];
        endforeach;

        $arService['days'] = ceil($arService['time'] / $hourPerDay * $timeMultiplier);

    endforeach;
    unset($arService);

    ?>
</table>
<br />
<p style="font-size: 15pt; font-weight: bold;">Стоимость этапов:</p>
<br />
<table border="1" cellspacing="0" cellpadding="3"><?
    $arTotal = [
        'days' => 0,
        'price_ex' => 0
    ];
    foreach ($arParams['services'] as $key => $arService):?>
        <tr>
            <td width="7%" style="font-size: 10pt;" valign="middle" align="center"><?=($key+1)?></td>
            <td width="45%" style="font-size: 10pt;" valign="middle"><?=($arService['title'] ?: $arService['name'])?></td>
            <td width="21%" style="font-size: 10pt;" valign="middle" align="center"><?=$arService['days']?> <?=\Mn\Estimate\Helper::declOfNum((int) $arService['days'], ['рабочий день', 'рабочих дня', 'рабочих дней'])?></td>
            <td width="27%" style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arService['total_ex'], 0, ',', ' ')?>,00 руб.</td>
        </tr><?
        $arTotal['days'] += $arService['days'];
        $arTotal['price_ex'] += $arService['total_ex'];
    endforeach;
    ?>
</table>
<br />
<p style="color: rgb(148,54,52); font-size: 10pt; font-weight: bold;">Порядок оплаты – 50% предоплата, 50% - по факту выполненных работ</p>
<br />
<p style="color: rgb(148,54,52); font-size: 10pt; font-weight: bold;">ИТОГО</p>
<br />
<table border="1" cellspacing="0" cellpadding="3">
    <tr>
        <th width="7%" bgcolor="#f2f2f2" style="font-size: 10pt;" align="center">Этап</th>
        <th width="45%" bgcolor="#f2f2f2" style="font-size: 10pt;">Задание</th>
        <th width="21%" bgcolor="#f2f2f2" style="font-size: 10pt;">Сроки</th>
        <th width="27%" bgcolor="#f2f2f2" style="font-size: 10pt;">Стоимость</th>
    </tr>
    <tr>
        <td style="font-size: 10pt;" valign="middle" align="center">1</td>
        <td style="font-size: 10pt;" valign="middle">Разработка сайта на системе управления 1С Bitrix</td>
        <td style="font-size: 10pt;" valign="middle"><?=$arTotal['days']?> <?=\Mn\Estimate\Helper::declOfNum((int) $arTotal['days'], ['рабочий день', 'рабочих дня', 'рабочих дней'])?></td>
        <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arTotal['price_ex'], 0, ',', ' ')?> руб. 00 коп.</td>
    </tr>
    <?
    $num = 2;

    $arTotal['discount'] = 0;
    if ($arParams['item']['discount']):

        if (preg_match('/^(\d+(\.\d+)?)(%?)$/', $arParams['item']['discount'], $arMatches)):

            if ((float) $arMatches[1] > 0):
                if (!empty($arMatches[3])) {
                    $isPercentSale = true;
                    $arTotal['discount'] = $arTotal['price_ex'] * ((float) $arMatches[1]/100);
                } else {
                    $arTotal['discount'] = (float) $arMatches[1];
                }
                ?>
                <tr>
                    <td style="font-size: 10pt;" valign="middle" align="center"><?=$num?></td>
                    <td style="font-size: 10pt;" valign="middle">Скидка <?=($isPercentSale ? $arParams['item']['discount'] : '')?></td>
                    <td style="font-size: 10pt;" valign="middle"></td>
                    <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format($arTotal['discount'], 2, ' руб. ', ' ')?> коп.</td>
                </tr>
                <?
                $num++;
            endif;
        endif;

    endif;

    if ((int) $arParams['item']['vat'] > 0):
        ?>
        <tr>
            <td style="font-size: 10pt;" valign="middle" align="center"><?=$num?></td>
            <td style="font-size: 10pt;" valign="middle">Включая НДС <?=$vatPercent?>%</td>
            <td style="font-size: 10pt;" valign="middle"></td>
            <td style="font-size: 10pt;" valign="middle" align="right"><?=number_format(($arTotal['price_ex'] - $arTotal['discount']) * ($vatPercent/100), 2, ' руб. ', ' ')?> коп.</td>
        </tr>


    <?
    endif;?>
    <tr>
        <td style="font-size: 15pt; font-weight: bold;" valign="middle" colspan="2">Итого</td>
        <td style="font-size: 10pt; font-weight: bold;" valign="middle"><?=$arTotal['days']?> <?=\Mn\Estimate\Helper::declOfNum((int) $arTotal['days'], ['рабочий день', 'рабочих дня', 'рабочих дней'])?></td>
        <td style="font-size: 10pt; font-weight: bold;" valign="middle" align="right"><?=number_format(($arTotal['price_ex'] - $arTotal['discount']), 2, ' руб. ', ' ')?> коп.</td>
    </tr>
</table>
<table cellpadding="20">
    <tr><td></td></tr>
</table>
<table border="1" cellspacing="0" cellpadding="3">
    <tr>
        <td width="50%" style="font-size: 10pt; font-weight: bold;" align="center">ЗАКАЗЧИК</td>
        <td width="50%" style="font-size: 10pt; font-weight: bold;" align="center">ИСПОЛНИТЕЛЬ</td>
    </tr>
    <tr>
        <td width="50%" style="font-size: 11pt;">
            <table cellpadding="3">
                <tr><td></td></tr>
            </table>
            <table width="70%">
                <tr>
                    <td>
                        <span>Генеральный директор</span>
                        <table cellpadding="20">
                            <tr><td></td></tr>
                        </table>
                        <hr />
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%" style="font-size: 11pt;">
            <table cellpadding="3">
                <tr><td></td></tr>
            </table>
            <table width="70%">
                <tr>
                    <td>
                        <span>Генеральный директор</span>
                        <table cellpadding="20">
                            <tr><td></td></tr>
                        </table>
                        <hr />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


