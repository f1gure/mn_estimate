<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Mn\Estimate\Orm\ServicesTable;

if (!Loader::includeModule('mn.estimate')) {
    return;
}

?>
    <tr>
        <td>
            <input type="text" class="positions-table__input bx-crm-edit-input" name="positions[name]" value="" />
        </td>
        <td>
            <select class="positions-table__input bx-crm-edit-input bx-crm-edit-input-small" name="" onChange="">
                <option value=""></option><?
                foreach ($arServices as $id => $value):?>
                    <option value="<?=$id?>"><?=$value?></option><?
                endforeach;
                ?>
            </select>
        </td>
        <td>
            <select class="positions-table__input bx-crm-edit-input bx-crm-edit-input-small" name="positions[position_id]" disabled>
                <option value=""></option>
            </select>
        </td>
        <td>
            <div class="positions-table__nowrap">
                <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[count]" value="0" disabled /> <span>ч</span>
            </div>
        </td>
        <td>
            <div class="positions-table__nowrap">
                <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[price]" value="0" disabled /> <span>руб</span>
            </div>
        </td>
        <td>
            <div class="positions-table__nowrap">
                <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[count_ex]" value="0" disabled /> <span>ч</span>
            </div>
        </td>
        <td>
            <div class="positions-table__nowrap">
                <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[price_ex]" value="0" disabled /> <span>руб</span>
            </div>
        </td>
    </tr>
<?
