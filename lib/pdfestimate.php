<?php


namespace Mn\Estimate;

require(__DIR__ . '/../include/tcpdf_min/tcpdf.php');
use TCPDF;

class PdfEstimate extends TCPDF {

    public function Header() {
        // TODO: fix cyrillic date
        //FormatDate('d F Y') . ' года';
        $this->setFontSubsetting(true);
        $this->SetTextColor(150);
        $this->SetFont('freesans', '', 12);
        $this->Cell(0, 12, 'Смета действительна на ' . date('d.m.Y'), 0, false, 'R', 0, '', 0, false, 'T', 'T');
    }
}
