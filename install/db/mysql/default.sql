
insert into mn_estimate_services (name, title, sort) values
    ('Дизайн', 'Дизайн', 100),
    ('Верстка', 'Верстка', 200),
    ('Программирование', 'Программирование', 300),
    ('Контент', 'Контент', 400);

insert into mn_estimate_units (name, label) values
    ('часы', 'ч'),
    ('шт', 'шт'),
    ('на 1000 знаков', 'x 1000 зн');

insert into mn_estimate_positions (name, service_id, unit_id, price, price_ex) values
    ('Мастер баннер', 1, 2, 1000, 2500),
    ('Дизайн сайта', 1, 1, 500, 1550),
    ('Программирование сайта', 3, 1, 900, 1850),
    ('Верстка сайта', 2, 1, 700, 1375),
    ('Контент сайта', 4, 1, 600, 1250);

