<?php

namespace Mn\Estimate\Orm;
use Bitrix\Main,
    Bitrix\Main\ORM,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ServicesTable extends Main\Entity\DataManager {

    public static function getTableName() {

        return 'mn_estimate_services';
    }

    public static function getMap() {

        return [
            'id' => new ORM\Fields\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_SERVICES_ID')
            ]),
            'name' => new ORM\Fields\StringField('name', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_SERVICES_NAME')
            ]),
            'title' => new ORM\Fields\StringField('title', [
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_SERVICES_TITLE')
            ]),
            'sort' => new ORM\Fields\IntegerField('sort', [
                'default_value' => 500,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_SERVICES_SORT')
            ])
        ];
    }

    public static function validateName() {

        return [
            new ORM\Fields\Validators\LengthValidator(null, 255)
        ];
    }

    public static function getConcatList($arFilter = []) {

        $arResult = [];

        $res = self::getList([
            'select' => [
                'id', 'name'
            ],
            'order' => ['id' => 'ASC'],
            'filter' => $arFilter
        ]);
        while ($arItem = $res->fetch()) {
            $arResult[$arItem['id']] = $arItem['name'];
        }

        return $arResult;
    }
}
