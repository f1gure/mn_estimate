<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => array(
            'positions' => array(
                'NAME' => Loc::getMessage('CP_MN_ESTIMATE_SEF_MODE_POSITIONS_URL_TEMPLATE'),
                'DEFAULT' => 'positions/',
                'VARIABLES' => []
            ),
            'position_edit' => array(
                'NAME' => Loc::getMessage('CP_MN_ESTIMATE_SEF_MODE_POSITION_EDIT_URL_TEMPLATE'),
                'DEFAULT' => 'positions/edit/#POSITION_ID#/',
                'VARIABLES' => ['POSITION_ID']
            ),
            'edit' => array(
                'NAME' => Loc::getMessage('CP_MN_ESTIMATE_SEF_MODE_EDIT_URL_TEMPLATE'),
                'DEFAULT' => 'edit/#ITEM_ID#/',
                'VARIABLES' => ['ITEM_ID']
            )
        )
    )
);
