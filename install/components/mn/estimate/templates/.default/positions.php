<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

$APPLICATION->SetTitle(Loc::getMessage('CT_MN_ESTIMATE_POSITIONS_TITLE'));

$urlTemplates = [
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['position_edit']
];

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'title',
    array(
        'TOOLBAR_ID' => 'MN_ESTIMATE_POSITIONS_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => Loc::getMessage('CT_MN_ESTIMATE_POSITIONS_ADD'),
                'TITLE' => Loc::getMessage('CT_MN_ESTIMATE_POSITIONS_ADD'),
                'LINK' => CComponentEngine::makePathFromTemplate($urlTemplates['EDIT'], ['POSITION_ID' => 0]),
                'ICON' => 'btn-add',
            ),
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'mn:estimate.position.list',
    '',
    array(
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['positions'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
