<?
$MESS['MN_ESTIMATE_INSTALL_NAME'] = 'Модуль генерации смет';
$MESS['MN_ESTIMATE_INSTALL_DESCRIPTION'] = 'Модуль для генерации смет (внутренних и внешних)';
$MESS['MN_ESTIMATE_INSTALL_PARTNER_NAME'] = 'Artur G';
$MESS['MN_ESTIMATE_INSTALL_PARTNER_URI'] = 'https://dev.1c-bitrix.ru/learning/resume.php?ID=25993386-142929';
$MESS['MN_ESTIMATE_INSTALL_TITLE'] = 'Установка модуля #MODULE_NAME#';
$MESS['MN_ESTIMATE_UNINSTALL_TITLE'] = 'Удаление модуля #MODULE_NAME#';
