<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CC_MN_ESTIMATE_ITEM_EDIT_TITLE_DEFAULT'] = 'Генерация сметы';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_NO_MODULE_CRM'] = 'Модуль "CRM" не установлен.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_NO_MODULE'] = 'Модуль "Генерация смет" не установлен.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_NOT_REALIZED'] = 'Функционал не доступен';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_ITEM_NOT_FOUND'] = 'Смета не существует';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_SHOW_TITLE'] = 'Смета №#ID# &mdash; #NAME#';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_NAME'] = 'Название для сметы не задано.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_COMPANY'] = 'Не указана компания для сметы.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_UNKNOWN_COMPANY'] = 'Указанная компания не существует.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_DOMAIN'] = 'Домен';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_POSITIONS'] = 'Не заданы позиции для сметы.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_POSITIONS_ID'] = 'Не указан ID позиции.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_UNKNOWN_POSITIONS'] = 'Указананная позиция не найдена.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_PRICE'] = 'Не указана внутренняя цена для позиции.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_COUNT'] = 'Не указано внутреннее количество для позиции.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_PRICE_EX'] = 'Не указана внешняя цена для позиции.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_COUNT_EX'] = 'Не указано внешнее количество для позиции.';
$MESS['CC_MN_ESTIMATE_ITEM_EDIT_NAME_PATTERN'] = 'Смета на разработку сайта #DOMAIN# на системе управления Битрикс (БУС)';
