<?php

defined('B_PROLOG_INCLUDED') || die;

use Mn\Estimate\Orm\ItemsTable;
use Mn\Estimate\Orm\ServicesTable;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use Bitrix\Main\Grid;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;

class CMnEstimateItemListComponent extends CBitrixComponent {

    const GRID_ID = 'MN_ESTIMATE_ITEM_LIST';
    const SELECT_FIELDS = array('id', 'name', 'company_id', 'company_name' => 'company.TITLE', 'domain', 'estimate_id', 'estimate_ex_id', 'updated_at', 'updated_by', 'updated_user_name' => 'updated_at_user.SHORT_NAME', 'updated_user_id' => 'updated_at_user.ID');
    const SORTABLE_FIELDS = array('id', 'name', 'domain');
    const FILTERABLE_FIELDS = array('id', 'company_id', 'name', 'domain');
    const SUPPORTED_ACTIONS = array('delete');
    const SUPPORTED_SERVICE_ACTIONS = array('GET_ROW_COUNT');

    private static $headers;
    private static $filterFields;
    private static $filterPresets;

    private static $serviceList;

    public function __construct(CBitrixComponent $component = null) {

        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_NO_MODULE_CRM'));
            return;
        }

        if (!Loader::includeModule('mn.estimate')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_NO_MODULE'));
            return;
        }

        parent::__construct($component);

        self::$serviceList = $this->getServiceList();

        self::$headers = array(
            array(
                'id' => 'id',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_ID'),
                'sort' => 'id',
                'first_order' => 'desc',
                'type' => 'int'
            ),
            array(
                'id' => 'name',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_NAME'),
                'sort' => 'name',
                'default' => true
            ),
            array(
                'id' => 'company',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_COMPANY'),
                'default' => true
            ),
            array(
                'id' => 'domain',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_DOMAIN'),
                'sort' => 'domain',
                'default' => true
            ),
            array(
                'id' => 'estimate_id',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_ESTIMATE'),
                'default' => true
            ),
            array(
                'id' => 'estimate_ex_id',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_ESTIMATE_EX'),
                'default' => true
            ),
            array(
                'id' => 'updated_at',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_UPDATED_AT'),
                'default' => true
            ),
            array(
                'id' => 'updated_by',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_COLUMN_UPDATED_BY'),
                'default' => true
            )
        );

        self::$filterFields = array(
            array(
                'id' => 'id',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_FILTER_ID')
            ),
            array(
                'id' => 'name',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_FILTER_NAME'),
                'default' => true
            ),
            array(
                'id' => 'company_id',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_FILTER_COMPANY'),
                'type' => 'custom_entity',
                'selector' => array(
                    'TYPE' => 'crm_entity',
                    'DATA' => array(
                        'ID' => 'company',
                        'FIELD_ID' => 'company_id',
                        'ENTITY_TYPE_NAMES' => [CCrmOwnerType::CompanyName],
                        'IS_MULTIPLE' => false
                    )
                ),
                'default' => true
            ),
            array(
                'id' => 'domain',
                'name' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_FILTER_DOMAIN'),
                'default' => true
            )
        );

        self::$filterPresets = array();
    }

    public function executeComponent() {

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $grid = new Grid\Options(self::GRID_ID);

        $gridSort = $grid->getSorting();
        $sort = array_filter(
            $gridSort['sort'],
            function ($field) {
                return in_array($field, self::SORTABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        if (empty($sort)) {
            $sort = array('id' => 'desc');
        }

        $gridFilter = new Filter\Options(self::GRID_ID, self::$filterPresets);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);
        $gridFilterValues = array_filter(
            $gridFilterValues,
            function ($fieldName) {
                return in_array($fieldName, self::FILTERABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );

        if (isset($gridFilterValues['name'])) {

            $gridFilterValues['name'] = '%' . $gridFilterValues['name'] . '%';
        }
        if (isset($gridFilterValues['domain'])) {

            $gridFilterValues['domain'] = '%' . $gridFilterValues['domain'] . '%';
        }

        $this->processGridActions($gridFilterValues);
        $this->processServiceActions($gridFilterValues);

        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $pager->setRecordCount(ItemsTable::getCount($gridFilterValues));
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }

        $arItems = $this->getItems([
            'filter' => $gridFilterValues,
            'limit' => $pager->getLimit(),
            'offset' => $pager->getOffset(),
            'order' => $sort,
            'select' => self::SELECT_FIELDS
        ]);

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        $this->arResult = array(
            'GRID_ID' => self::GRID_ID,
            'ITEMS' => $arItems,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
            'SORT' => $sort,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
            'SERVICE_URL' => $requestUri->getUri(),
        );

        $this->includeComponentTemplate();
    }

    private function getItems($arParams = array()) {

        $dbRes = ItemsTable::getList($arParams);
        $arItems = $dbRes->fetchAll();

        return $arItems;
    }

    private function getServiceList() {

        $arServices = [];
        $dbRes = ServicesTable::getList(['select' => ['id', 'name']]);
        while ($arService = $dbRes->fetch()) {
            $arServices[$arService['id']] = $arService['name'];
        }

        return $arServices;
    }

    private function processGridActions($currentFilter) {

        if (!check_bitrix_sessid()) {

            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $action = $request->get('action_button_' . self::GRID_ID);

        if (!in_array($action, self::SUPPORTED_ACTIONS, true)) {

            return;
        }

        $allRows = $request->get('action_all_rows_' . self::GRID_ID) === 'Y';
        if ($allRows) {
            $dbRes = ItemsTable::getList(array(
                'filter' => $currentFilter,
                'select' => array('ID'),
            ));
            $itemIds = array();
            foreach ($dbRes as $item) {
                $itemIds[] = $item['ID'];
            }
        } else {
            $itemIds = $request->get('ID');
            if (!is_array($itemIds)) {
                $itemIds = array();
            }
        }

        if (empty($itemIds)) {
            return;
        }

        switch ($action) {
            case 'delete':
                foreach ($itemIds as $id) {
                    ItemsTable::delete($id);
                }
                break;

            default:
                break;
        }
    }

    private function processServiceActions($currentFilter) {

        global $APPLICATION;

        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $params = $request->get('PARAMS');

        if (empty($params['GRID_ID']) || $params['GRID_ID'] !== self::GRID_ID) {
            return;
        }

        $action = $request->get('ACTION');

        if (!in_array($action, self::SUPPORTED_SERVICE_ACTIONS, true)) {
            return;
        }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        switch ($action) {
            case 'GET_ROW_COUNT':
                $count = ItemsTable::getCount($currentFilter);
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => Loc::getMessage('CC_MN_ESTIMATE_ITEM_LIST_ROW_COUNT', array('#COUNT#' => $count))
                    )
                ));
                break;

            default:
                break;
        }

        die;
    }
}
