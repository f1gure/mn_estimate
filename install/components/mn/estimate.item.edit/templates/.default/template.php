<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Mn\Estimate\Orm\ServicesTable;
use Mn\Estimate\Orm\UnitsTable;

/** @var CBitrixComponentTemplate $this */

/** @var ErrorCollection $errors */
$errors = $arResult['ERRORS'];

foreach ($errors as $error) {
    /** @var Error $error */
    ShowError($error->getMessage());
}
ob_start();

include ('positions.php');

$positionsFieldValue = ob_get_contents();
ob_end_clean();

ob_start();
?>
<div style="display: inline-block;vertical-align: top;"><?
$APPLICATION->IncludeComponent('bitrix:crm.entity.selector',
    '',
    array(
        'ENTITY_TYPE' => 'COMPANY',
        'INPUT_NAME' => 'company_id',
        'INPUT_VALUE' => isset($arResult['ITEM']['company_id']) ? 'CO_' . $arResult['ITEM']['company_id'] : '',
        'FORM_NAME' => $arResult['FORM_ID'],
        'MULTIPLE' => 'N'
    ),
    $component,
    array('HIDE_ICONS' => 'Y')
);
?>
</div><?
$companySelectorValue = ob_get_contents();
ob_end_clean();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'edit',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'N',
        'SHOW_SETTINGS' => 'N',
        'TITLE' => $arResult['TITLE'],
        'IS_NEW' => $arResult['IS_NEW'],
        'DATA' => $arResult['ITEM'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_TAB1_NAME'),
                'title' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_TAB1_TITLE'),
                'display' => false,
                'fields' => array(
                    array(
                        'id' => 'section_item',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_SECTION'),
                        'type' => 'section'
                    ),
                    array(
                        'id' => 'name',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_NAME'),
                        'type' => 'text',
                        'value' => $arResult['ITEM']['name']
                    ),
                    array(
                        'id' => 'company_id',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_COMPANY'),
                        'type' => 'custom',
                        'value' => $companySelectorValue
                    ),
                    array(
                        'id' => 'domain',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_DOMAIN'),
                        'type' => 'text',
                        'value' => $arResult['ITEM']['domain'],
                        'required' => true,
                        'params' => ['required' => 'required']
                    ),
                    array(
                        'id' => 'discount',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_DISCOUNT'),
                        'type' => 'text',
                        'value' => $arResult['ITEM']['discount']
                    ),
                    array(
                        'id' => 'vat',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_VAT'),
                        'type' => 'checkbox',
                        'value' => $arResult['ITEM']['vat'] ? 'Y' : 'N'
                    ),
                    array(
                        'id' => 'positions',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_EDIT_FIELD_POSITIONS'),
                        'type' => 'vertical_container',
                        'value' => $positionsFieldValue
                    )
                )
            ),
        ),
        'BUTTONS' => array(
            'back_url' => $arResult['BACK_URL'],
            'standard_buttons' => true,
            'wizard_buttons' => false
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
?>
<script type="text/javascript">

    var estimateEditCompanyChanged = function (companyObj) {

        var form = document.getElementById('form_<?=$arResult['FORM_ID']?>');
        var domainField = form.querySelector('input[name="domain"]');

        if (!!companyObj.company[0].id) {

            var companyId = parseInt(companyObj.company[0].id.replace('CO_', ''));
            if (form && domainField/* && !domainField.value*/) {

                BX.ajax({
                    url: '<?=($templateFolder . '/ajax.php')?>',
                    data: {
                        action: 'getDomainFromCompany',
                        company_id: companyId
                    },
                    method: 'POST',
                    dataType: 'json',
                    onsuccess: function (data) {
                        if (!!data && data.success) {
                            if (!!data.result) {
                                domainField.value = data.result;
                            }
                        } else {
                            if (!!data && !!data.message) {
                                console.error(data.message);
                            }
                        }
                    },
                    onfailure: function () {

                    }
                });
            }
        }
    };

    if (!!obCrm) {
        setTimeout(function () {
            obCrm['crm-form-<?=strtolower(str_replace('_', '-', $arResult['FORM_ID']))?>-company-id-open'].AddOnSaveListener(estimateEditCompanyChanged);
        }, 100);
    }
</script>
