<?php

namespace Mn\Estimate\Orm;
use Bitrix\Main,
    Bitrix\Main\ORM,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class UnitsTable extends Main\Entity\DataManager {

    public static function getTableName() {

        return 'mn_estimate_units';
    }

    public static function getMap() {

        return [
            'id' => new ORM\Fields\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_UNITS_ID')
            ]),
            'name' => new ORM\Fields\StringField('name', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_UNITS_NAME')
            ]),
            'label' => new ORM\Fields\StringField('label', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_UNITS_LABEL')
            ]),
            'time_ratio' => new ORM\Fields\FloatField('time_ratio', [
                'default_value' => 1,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_UNITS_TIME_RATIO')
            ])
        ];
    }

    public static function validateName() {

        return [
            new ORM\Fields\Validators\LengthValidator(null, 255)
        ];
    }
}
