<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CT_MN_ESTIMATE_POSITION_LIST_EDIT_TITLE'] = 'Редактировать позицию услуги';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_EDIT_TEXT'] = 'Редактировать';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_DELETE_TITLE'] = 'Удалить позицию услуги';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_DELETE_TEXT'] = 'Удалить';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_DELETE_DIALOG_TITLE'] = 'Удалить позицию услуги';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить позицию услуги?';
$MESS['CT_MN_ESTIMATE_POSITION_LIST_DELETE__DIALOG_BUTTON'] = 'Удалить';
