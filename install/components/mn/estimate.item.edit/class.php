<?php
defined('B_PROLOG_INCLUDED') || die;

use Mn\Estimate\Orm\ItemsTable;
use Mn\Estimate\Orm\PositionsTable;
use Mn\Estimate\Orm\PositionItemTable;
use Mn\Estimate\Helper as EstimateHelper;
use Mn\Estimate\PdfGen;
use Mn\Estimate\Orm\ServicesTable;
use Mn\Estimate\Orm\UnitsTable;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class CMnEstimateItemEditComponent extends CBitrixComponent {
    const FORM_ID = 'MN_ESTIMATE_ITEM_EDIT';

    private $errors;
    private $pdfGenObj = null;

    public function __construct(CBitrixComponent $component = null) {

        parent::__construct($component);

        if (!Loader::includeModule('mn.estimate')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_NO_MODULE'));
            return;
        }

        $this->errors = new ErrorCollection();
        $this->pdfGenObj = new PdfGen();

        CBitrixComponent::includeComponentClass('mn:estimate.item.list');
    }

    public function executeComponent() {

        global $APPLICATION, $USER;

        $title = Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_TITLE_DEFAULT');

        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_NO_MODULE_CRM'));
            return;
        }

        $arItem = array(
            'name' => '',
            'company_id' => 0,
            'domain' => '',
            'vat' => 0,
            'discount' => '0',
            'updated_at' => new \Bitrix\Main\Type\DateTime(),
            'updated_by' => $USER->GetID(),
            'estimate' => 0,
            'estimate_ex' => 0,
            'positions' => []
        );

        if ((int) $this->arParams['ITEM_ID'] > 0) {

            // TODO: Realize edit
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_NOT_REALIZED'));
            return;

            $dbRes = ItemsTable::getById($this->arParams['ITEM_ID']);
            $arItem = $dbRes->fetch();

            if (empty($arItem)) {
                ShowError(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_ITEM_NOT_FOUND'));
                return;
            }
        }

        if (!empty($arItem['id'])) {
            $title = Loc::getMessage(
                'CC_MN_ESTIMATE_ITEM_EDIT_SHOW_TITLE',
                array(
                    '#ID#' => $arItem['id'],
                    '#NAME#' => $arItem['name']
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $savedId = $this->processSave($arItem);
            if ($savedId > 0) {
                LocalRedirect($this->getRedirectUrl($savedId));
            }

            $submitted = $this->getSubmittedItem();
            $arItem = array_merge($arItem, $submitted);
        }

        $this->arResult = [
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => CMnEstimateItemListComponent::GRID_ID,
            'IS_NEW' => empty($arItem['id']),
            'TITLE' => $title,
            'ITEM' => $arItem,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        ];

        $this->includeComponentTemplate();
    }

    private function processSave($initialItem) {

        $submittedItem = $this->getSubmittedItem();

        $item = array_merge($initialItem, $submittedItem);

        $item['positions'] = self::normalizePositionsArray($item['positions']);

        $this->errors = self::validate($item);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        $positions = $item['positions'];
        unset($item['positions']);

        $item['domain'] = EstimateHelper::getDomainFromUrl($item['domain']);
        $item['discount'] = EstimateHelper::discountValueClear($item['discount']);

        if (!empty($item['id'])) {
            // TODO: realize update
            return false;
            $result = ItemsTable::update($item['id'], $item);
            $this->addOrUpdateItemPositions($item['id'], $positions);
        } else {

            // TODO: generate pdf
            $item['estimate_id'] = 0;
            $item['estimate_ex_id'] = 0;
            $item['name'] = $item['name'] ?: Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_NAME_PATTERN', [
                '#DOMAIN#' => $item['domain']
            ]);
            
            $result = ItemsTable::add($item);
            if ($result->isSuccess() && $result->getId()) {
                $id = (int) $result->getId();
                $this->addItemPositions($result->getId(), $positions);

                $estimateId = $this->pdfGenObj->generate($id);
                $estimateExId = $this->pdfGenObj->generate($id,'estimate_ex');

                ItemsTable::update($id, [
                    'estimate_id' => $estimateId,
                    'estimate_ex_id' => $estimateExId,
                ]);
            }
        }

        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        return $result->isSuccess() ? $result->getId() : false;
    }

    private function normalizePositionsArray($arPositions) {

        $arResult = [];

        foreach ($arPositions as $code => $arItem) {

            foreach ($arItem as $num => $value) {

                $arResult[$num][$code] = $value;
            }
        }

        foreach ($arResult as $key => $arItem) {

            if (count($arItem) === 1 && isset($arItem['name'])) {
                unset($arResult[$key]);
            }
        }

        return array_values($arResult);
    }

    private function addItemPositions($itemId, $arPositions = []) {

        if (!empty($arPositions)) {

            foreach ($arPositions as $arPosition) {

                $arParams = array_merge(['item_id' => $itemId], $arPosition);
                $res = PositionItemTable::add($arParams);
            }
        }
        return true;
    }

    private function addOrUpdateItemPositions($itemId, $arPositions = []) {

        // TODO: Realize
        return false;

        if (!empty($arPositions)) {

            foreach ($arPositions as $position) {


            }

            $arParams = array_merge(['item_id' => $itemId], $arPositions);
            $res = PositionItemTable::add($arParams);
        }
        return true;
    }

    private function getSubmittedItem() {

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $company_id = (int) str_replace('CO_', '', $request->get('company_id')) ?: 0;

        $submittedItem = [
            'name' => $request->get('name'),
            'company_id' => $company_id,
            'domain' => $request->get('domain'),
            'vat' => $request->get('vat') === 'Y' ? 1 : 0,
            'discount' => EstimateHelper::discountValueClear($request->get('discount')),
            'positions' => $request->get('positions')
        ];

        return $submittedItem;
    }

    private static function validate($arItem) {

        $errors = new ErrorCollection();

        /*if (empty($arItem['name'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_NAME')));
        }*/

        if (empty($arItem['company_id'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_COMPANY')));
        } else {
            $res = \Bitrix\Crm\CompanyTable::getById($arItem['company_id']);
            if ($res->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_UNKNOWN_COMPANY')));
            }
        }

        if (empty($arItem['domain'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_DOMAIN')));
        }

        if (empty($arItem['positions'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_POSITIONS')));
        } else {

            foreach ($arItem['positions'] as $position) {

                if (empty($position['position_id'])) {
                    $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_EMPTY_POSITIONS_ID')));
                } else {
                    $res = PositionsTable::getById($position['position_id']);
                    if ($res->getSelectedRowsCount() <= 0) {
                        $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_UNKNOWN_POSITIONS')));
                    }
                }

                foreach (['price', 'count', 'price_ex', 'count_ex'] as $code) {

                    if (empty($position[$code]) || (int) $position[$code] <= 0) {
                        $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_ITEM_EDIT_VALIDATE_ERROR_' . strtoupper($code))));
                    }
                }
            }
        }

        return $errors;
    }

    private static function isFormSubmitted() {

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedId = null) {

        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedId) && $request->offsetExists('apply')) {
            return $this->arParams['SEF_FOLDER'];
            /*return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                ['ITEM_ID' => $savedId]
            );*/
        } elseif (!empty($savedId) && $request->offsetExists('saveAndAdd')) {
            return $this->arParams['SEF_FOLDER'];
            /*return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                ['ITEM_ID' => 0]
            );*/
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedId) && $request->offsetExists('saveAndView')) {
            return $this->arParams['SEF_FOLDER'];
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}
