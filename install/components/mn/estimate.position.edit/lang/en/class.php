<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_NAME'] = 'Название для позиции услуги не задано.';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_EMPTY_SERVICE'] = 'Не указана услуга.';
$MESS['CC_EMN_STIMATE_POSITION_EDIT_VALIDATE_ERROR_UNKNOWN_SERVICE'] = 'Указанная услуга не существует.';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_EMPTY_UNIT'] = 'Не указана единица измерения';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_UNKNOWN_UNIT'] = 'Указанная единица измерения не существует.';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_PRICE'] = 'Не задана внутренняя цена';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_PRICE_EX'] = 'Не задана внешняя цена';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_TITLE_DEFAULT'] = 'Позиция услуги';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_SHOW_TITLE'] = 'Позиция услуги №#ID# &mdash; #NAME#';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_ITEM_NOT_FOUND'] = 'Позиция услуги не существует';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_NO_MODULE_CRM'] = 'Модуль "CRM" не установлен.';
$MESS['CC_MN_ESTIMATE_POSITION_EDIT_NO_MODULE'] = 'Модуль "Генерация смет" не установлен.';
