<?php

namespace Mn\Estimate\Orm;
use Bitrix\Main,
    Bitrix\Main\ORM,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ItemsTable extends Main\Entity\DataManager {

    public static function getTableName() {

        return 'mn_estimate_items';
    }

    public static function getMap() {

        return [
            'id' => new ORM\Fields\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_ID')
            ]),
            'name' => new ORM\Fields\StringField('name', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('N_ESTIMATE_ENTITY_ITEMS_NAME')
            ]),
            'company_id' => new ORM\Fields\IntegerField('company_id', [
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_COMPANY_ID')
            ]),
            'company' => new ORM\Fields\Relations\Reference(
                'company',
                \Bitrix\Crm\CompanyTable::class,
                array('=this.company_id' => 'ref.ID'),
                array('join_type' => 'left')
            ),
            'domain' => new ORM\Fields\StringField('domain', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_DOMAIN')
            ]),


            'vat' => new ORM\Fields\BooleanField('vat', array(
                'values' => [0, 1],
                'default_value' => 0,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_VAT'),
            )),

            'discount' => new ORM\Fields\StringField('discount', array(
                'default_value' => '0',
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_DISCOUNT'),
            )),

            'estimate_id' => new ORM\Fields\IntegerField('estimate_id', [
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_ESTIMATE_ID')
            ]),
            'estimate_ex_id' => new ORM\Fields\IntegerField('estimate_ex_id', [
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_ESTIMATE_EX_ID')
            ]),
            'updated_by' => new ORM\Fields\IntegerField('updated_by', [
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_UPDATED_BY')
            ]),
            'updated_at_user' => new ORM\Fields\Relations\Reference(
                'updated_at_user',
                \Bitrix\Main\UserTable::class,
                array('=this.updated_by' => 'ref.ID'),
                array('join_type' => 'left')
            ),
            'updated_at' => new ORM\Fields\DatetimeField('updated_at', [
                'default_value' => function() {
                    return new Main\Type\DateTime();
                },
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_ITEMS_UPDATED_AT')
            ])
        ];
    }

    public static function validateName() {

        return [
            new ORM\Fields\Validators\LengthValidator(null, 255)
        ];
    }
}
