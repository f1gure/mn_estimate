
create table if not exists mn_estimate_services
(
	id int(11) not null auto_increment,
	name varchar(255) not null,
	title varchar(255),
	sort int(11) default 500,

	primary key (id)
);

create table if not exists mn_estimate_units
(
	id int(11) not null auto_increment,
	name varchar(255) not null,
	label varchar(255) not null,
	time_ratio float(11) default 1,

	primary key (id)
);

create table if not exists mn_estimate_positions
(
	id int(11) not null auto_increment,
	service_id int(11) not null,
	name varchar(255) not null,
	unit_id int(11) not null,
	price int(11) not null,
	price_ex int(11) not null,
	updated_at datetime,
	updated_by int(18),

	primary key (id)
);

create table if not exists mn_estimate_position_item
(
	id int(11) not null auto_increment,
	position_id int(11) not null,
	item_id int(11) not null,
	name varchar(255) not null,
	price int(11) not null,
	count int(11) not null,
	price_ex int(11) not null,
	count_ex int(11) not null,

	primary key (id)
);

create table if not exists mn_estimate_items
(
	id int(11) not null auto_increment,
	name varchar(255) not null,
	company_id int(11) not null,
	domain varchar(255) not null,
	estimate_id int(11) not null,
	estimate_ex_id int(11) not null,
	vat int(1) not null default 0,
	discount varchar(255) not null default '0',
	updated_at datetime,
	updated_by int(18),

	primary key (id)
);
