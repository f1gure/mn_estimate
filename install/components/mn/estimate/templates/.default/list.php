<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

$APPLICATION->SetTitle(Loc::getMessage('CT_MN_ESTIMATE_LIST_TITLE'));

$urlTemplates = [
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit']
];

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'title',
    array(
        'TOOLBAR_ID' => 'MN_ESTIMATE_LIST_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => Loc::getMessage('CT_MN_ESTIMATE_LIST_POSITIONS'),
                'TITLE' => Loc::getMessage('CT_MN_ESTIMATE_LIST_POSITIONS'),
                'LINK' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['positions'],
                'ICON' => 'btn-link',
            ),
            array(
                'TEXT' => Loc::getMessage('CT_MN_ESTIMATE_LIST_ADD'),
                'TITLE' => Loc::getMessage('CT_MN_ESTIMATE_LIST_ADD'),
                'LINK' => CComponentEngine::makePathFromTemplate($urlTemplates['EDIT'], ['ITEM_ID' => 0]),
                'ICON' => 'btn-add',
            )
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'mn:estimate.item.list',
    '',
    array(
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
