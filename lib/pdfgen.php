<?php


namespace Mn\Estimate;

use Mn\Estimate\Orm\ItemsTable;
use Mn\Estimate\Orm\PositionItemTable;
use Mn\Estimate\Orm\ServicesTable;

class PdfGen {

    protected $filePath = false;
    protected $tmpFilePath = '/upload/tmpEstimateFile.pdf';
    protected $bxFileDir = 'mn-estimate';

    public function __construct() {


    }

    protected function callGenerateMutator($itemId, $type) {

        $strField = '';
        $type = str_replace(['-', '.'], '', $type);
        foreach (explode('_', $type) as $part) {
            $strField .= ucfirst($part);
        }

        $mutatorName = 'generate' . $strField . 'Type';

        if (is_callable([$this, $mutatorName])) {

            return $this->$mutatorName($itemId);
        }

        return false;
    }

    public function generate($itemId, $type = 'estimate') {

        $this->filePath = $this->callGenerateMutator($itemId, $type);

        return $this->saveFile($this->filePath);
    }

    protected function getEstimateParams($itemId) {

        $arResult = [];

        $arItem = ItemsTable::getById($itemId)->fetch();
        if (!empty($arItem)) {

            $resPositionsItem = PositionItemTable::getList([
                'filter' => [
                    'item_id' => $arItem['id']
                ],
                'select' => [
                    'id', 'name', 'price', 'count', 'price_ex', 'count_ex',
                    'position_name' => 'position.name',
                    'service_id' => 'position.service_id',
                    'unit_name' => 'position.unit.name',
                    'unit' => 'position.unit.label',
                    'count_ratio' => 'position.unit.time_ratio'
                ]
            ]);
            $arIssetServiceIds = [];

            while ($arPositionsItem = $resPositionsItem->fetch()) {

                if (!in_array((int) $arPositionsItem['service_id'], $arIssetServiceIds, true)) {
                    $arIssetServiceIds[] = (int) $arPositionsItem['service_id'];
                }

                $arItem['positions'][(int) $arPositionsItem['service_id']][] = [
                    'id' => $arPositionsItem['id'],
                    'name' => $arPositionsItem['name'],
                    'title' => $arPositionsItem['position_name'],
                    'service_id' => $arPositionsItem['service_id'],
                    'price' => $arPositionsItem['price'],
                    'count' => $arPositionsItem['count'],
                    'total' => $arPositionsItem['price'] * $arPositionsItem['count'],
                    'price_ex' => $arPositionsItem['price_ex'],
                    'count_ex' => $arPositionsItem['count_ex'],
                    'count_ratio' => (float) $arPositionsItem['count_ratio'] > 0 ? (float) $arPositionsItem['count_ratio'] : 1,
                    'total_ex' => $arPositionsItem['price_ex'] * $arPositionsItem['count_ex'],
                    'unit' => $arPositionsItem['unit'] ?: $arPositionsItem['unit_name'],
                    'unit_label' => $arPositionsItem['unit'],
                ];
            }

            $arResult['item'] = $arItem;

            $arResult['services'] = ServicesTable::getList([
                'filter' => [
                    'id' => $arIssetServiceIds
                ],
                'order' => [
                    'sort' => 'asc'
                ]
            ])->fetchAll();
        }

        return $arResult;
    }

    public function generateEstimateType($itemId) {

        $arParams = $this->getEstimateParams($itemId);

        $pdf = new PdfEstimate('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setFontSubsetting(true);
        $pdf->SetAuthor('b24_module_mn_estimate');
        $pdf->SetTitle('Смета для ' . $arParams['item']['name']);

        $pdf->SetHeaderMargin(10);
        $pdf->setPrintFooter(false);

        $pdf->SetMargins(15, 25, 15);
        $pdf->SetFont('freesans', '', 12);

        $pdf->AddPage();

        ob_start();
        include(__DIR__ . '/../include/templates/estimate.php');
        $html = ob_get_clean();
        ob_end_clean();
        //var_dump($html);

        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output($_SERVER['DOCUMENT_ROOT'] . $this->tmpFilePath, 'F');

        return $this->tmpFilePath;
    }

    public function generateEstimateExType($itemId) {

        $arParams = $this->getEstimateParams($itemId);

        $pdf = new PdfEstimate('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setFontSubsetting(true);
        $pdf->SetAuthor('b24_module_mn_estimate');
        $pdf->SetTitle('Смета для ' . $arParams['item']['name']);

        $pdf->SetHeaderMargin(10);
        $pdf->setPrintFooter(false);

        $pdf->SetMargins(15, 25, 15);
        $pdf->SetFont('freesans', '', 12);

        $pdf->AddPage();

        ob_start();
        include(__DIR__ . '/../include/templates/estimate_ex.php');
        $html = ob_get_clean();
        ob_end_clean();
        //var_dump($html);/

        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output($_SERVER['DOCUMENT_ROOT'] . $this->tmpFilePath, 'F');

        return $this->tmpFilePath;
    }

    protected function saveFile($filePath) {

        $file = $_SERVER['DOCUMENT_ROOT'] . $filePath;

        if (file_exists($file)) {

            $id = \CFile::SaveFile(\CFile::MakeFileArray($file), $this->bxFileDir);

            @unlink($file);

            return $id;
        }
    }
}
