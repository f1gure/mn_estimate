<?php


namespace Mn\Estimate;


class Helper {

    public static function getDomainFromUrl($domainName) {

        if (preg_match('/^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)/', $domainName, $arMatches)) {

            $domainName = $arMatches[1] ?: $domainName;
        }

        return $domainName;
    }

    public static function strtolower($str) {

        if (function_exists('mb_strtolower')) {

            return mb_strtolower($str);
        }

        return strtolower($str);
    }

    public static function declOfNum($number, $arTitles) {

        $cases = array (2, 0, 1, 1, 1, 2);
        return $arTitles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
    }

    public static function discountValueClear($value) {

        if (preg_match('/^\d+(\.\d+)?%?$/', $value)) {

            return $value;
        }

        return '0';
    }
}
