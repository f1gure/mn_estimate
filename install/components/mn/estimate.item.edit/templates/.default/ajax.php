<?
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Context;
use Bitrix\Main\Loader;

if (!Loader::includeModule('crm')) {
    die;
}

$context = Context::getCurrent();
$request = $context->getRequest();

$action = $request->get('action') ?: '';

$arResult = [
    'success' => false
];

if ($action) {

    switch($action) {

        case 'getDomainFromCompany':

            $companyId = (int) $request->get('company_id') ?: 0;
            $arResult['success'] = true;
            $arResult['result'] = '';

            if ($companyId > 0) {

                $arDomain = \Bitrix\Crm\FieldMultiTable::getList([
                    'select' => ['ID', 'VALUE'],
                    'filter' => [
                        'ENTITY_ID' => 'COMPANY',
                        'TYPE_ID' => 'WEB',
                        'ELEMENT_ID' => $companyId
                    ],
                    'order' => ['ID' => 'ASC']
                ])->fetch();

                if (!empty($arDomain['VALUE'])) {

                    $arResult['result'] = $arDomain['VALUE'];
                }
            }
            break;

        case 'getPositionsService':

            if (!Loader::includeModule('mn.estimate')) {
                $arResult['message'] = 'can\'t load mn.estimate module';
                break;
            }

            $serviceId = (int) $request->get('service_id') ?: 0;
            $arResult['success'] = true;

            $resPositions = Mn\Estimate\Orm\PositionsTable::getList([
                'filter' => [
                    'service_id' => $serviceId
                ],
                'select' => ['id', 'name'],
                'order' => ['id' => 'ASC']
            ]);

            ob_start();
            ?>
            <select class="positions-table__input bx-crm-edit-input bx-crm-edit-input-small" name="" onchange="changeEstimateItemPosition(this);">
                <option value=""></option><?
                    while ($arPosition = $resPositions->fetch()):
                        ?>
                        <option value="<?=$arPosition['id']?>"><?=$arPosition['name']?></option><?
                    endwhile;
                ?>
            </select>
            <?
            $resHtml = ob_get_contents();
            ob_end_clean();

            ?><?
            $arResult['html'] = $resHtml;
            break;

        case 'getPositionParams':

            if (!Loader::includeModule('mn.estimate')) {
                $arResult['message'] = 'can\'t load mn.estimate module';
                break;
            }

            $positionId = (int) $request->get('position_id') ?: 0;
            $arResult['success'] = true;

            $arPosition = Mn\Estimate\Orm\PositionsTable::getList([
                'filter' => [
                    'id' => $positionId
                ],
                'select' => [
                    'price', 'price_ex', 'unit_name' => 'unit.name'
                ]
            ])->fetch();

            if ($arPosition) {

                $arResult['result'] = [
                    'price' => $arPosition['price'],
                    'price_ex' => $arPosition['price_ex'],
                    'unit' => $arPosition['unit_name']
                ];

            } else {

                $arResult['result'] = [];
            }

        default:
            $arResult['message'] = 'action "' . $action . '" not supported';
    }
}

echo json_encode($arResult, true);
die();
