<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Mn\Estimate\Orm\ServicesTable;

if (!Loader::includeModule('mn.estimate')) {
    return;
}

$arPositions = !empty($arResult['ITEM']['positions']) ? $arResult['ITEM']['positions'] : [];

$arServices = ServicesTable::getConcatList();

?>
<div class="positions-table">
    <table>
        <thead>
            <tr>
                <th>
                    Название
                </th>
                <th>
                    Услуга
                </th>
                <th>
                    Позиция
                </th>
                <th>
                    Кол-во<br />
                    <span>(внутр)</span>
                </th>
                <th>
                    Цена<br />
                    <span>(внутр)</span>
                </th>
                <th>
                    Кол-во<br />
                    <span>(внешн)</span>
                </th>
                <th>
                    Цена<br />
                    <span>(внешн)</span>
                </th>
            </tr>
        </thead>
        <tbody><?
            // TODO:: Realize edit
            /*foreach ($arPositions as $arPosition):
                ?>
                <tr>
                    <td>
                        <input type="text" name="positions[name]" value="<?=($arPositions['name'] ?: '')?>" />
                    </td>
                    <td>
                        <select name="">
                            <option value=""></option><?
                            foreach ($arServices as $id => $value):?>
                                <option value="<?=$id?>"<?=(isset($arPositions['service_id']) && (int) $arPositions['service_id'] === (int) $id ? ' selected' : '')?>><?=$value?></option><?
                            endforeach;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select name="positions[position_id]">
                            <option value=""></option><?
                            foreach ($arServices as $id => $value):?>
                                <option value="<?=$id?>"<?=(isset($arPositions['service_id']) && (int) $arPositions['service_id'] === (int) $id ? ' selected' : '')?>><?=$value?></option><?
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr><?
            endforeach;*/?>
            <tr>
                <td>
                    <input type="text" class="positions-table__input bx-crm-edit-input" name="positions[name][]" value="" />
                </td>
                <td>
                    <select class="positions-table__input bx-crm-edit-input bx-crm-edit-input-small" name="" onchange="changeEstimateItemService(this)">
                        <option value=""></option><?
                        foreach ($arServices as $id => $value):?>
                            <option value="<?=$id?>"><?=$value?></option><?
                        endforeach;
                        ?>
                    </select>
                </td>
                <td data-insert-ajax="positions">
                    <select class="positions-table__input bx-crm-edit-input bx-crm-edit-input-small" name="positions[position_id][]" disabled>
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <div class="positions-table__nowrap">
                        <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[count][]" value="1" disabled data-empty-disabled="true" /> <span data-insert-ajax="unit">ч</span>
                    </div>
                </td>
                <td>
                    <div class="positions-table__nowrap">
                        <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[price][]" data-insert-ajax="price" value="0" disabled  data-empty-disabled="true"/> <span>руб</span>
                    </div>
                </td>
                <td>
                    <div class="positions-table__nowrap">
                        <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[count_ex][]" value="1" disabled data-empty-disabled="true" /> <span data-insert-ajax="unit">ч</span>
                    </div>
                </td>
                <td>
                    <div class="positions-table__nowrap">
                        <input class="positions-table__input positions-table__input_vsmall bx-crm-edit-input bx-crm-edit-input-small" type="number" name="positions[price_ex][]" data-insert-ajax="price_ex" value="0" disabled data-empty-disabled="true" /> <span>руб</span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="positions-table__add-row crm-client-selector-type-item-select" onclick="addEstimatePositionRow()">Добавить еще</div>
</div>
<script type="text/javascript">

    var rowTemplate = document.querySelector('.positions-table tbody tr').outerHTML;

    function addEstimatePositionRow() {

        document.querySelector('.positions-table table tbody').insertAdjacentHTML('beforeEnd', rowTemplate);

        return true;
    }

    function changeEstimateItemService(el) {

        var row = el.closest('tr');

        BX.ajax({
            url: '<?=($templateFolder . '/ajax.php')?>',
            data: {
                action: 'getPositionsService',
                service_id: el.value
            },
            method: 'POST',
            dataType: 'json',
            onsuccess: function (data) {
                if (!!data && data.success) {
                    row.querySelectorAll('input[data-empty-disabled="true"]').forEach(function(item) {item.setAttribute('disabled', 'disabled'); item.removeAttribute('required')});
                    if (!!data.html) {
                        row.querySelector('[data-insert-ajax="positions"]').innerHTML = data.html;
                    }
                } else {
                    if (!!data && !!data.message) {
                        console.error(data.message);
                    }
                }
            },
            onfailure: function () {}
        });

        return false;
    }

    function changeEstimateItemPosition(el) {

        var row = el.closest('tr');

        if (!!el.value) {
            el.setAttribute('name', 'positions[position_id][]');
        } else {
            el.setAttribute('name', '');
        }

        BX.ajax({
            url: '<?=($templateFolder . '/ajax.php')?>',
            data: {
                action: 'getPositionParams',
                position_id: el.value
            },
            method: 'POST',
            dataType: 'json',
            onsuccess: function (data) {
                if (!!data && data.success) {
                    if (!!data.result) {
                        row.querySelectorAll('input[disabled]').forEach(function(item) {item.removeAttribute('disabled'); item.setAttribute('required', 'required')});
                        for (var code in data.result) {
                            var item = row.querySelectorAll('[data-insert-ajax="' + code + '"]');
                            if (item) {
                                item.forEach(function(thisItem) {
                                   if (thisItem.tagName === 'INPUT') {
                                       thisItem.value = data.result[code];
                                   }  else {
                                       thisItem.innerHTML = data.result[code];
                                   }
                                });
                            }
                        }
                    }
                } else {
                    if (!!data && !!data.message) {
                        console.error(data.message);
                    }
                }
            },
            onfailure: function () {}
        });

        return false;
    }
</script><?
