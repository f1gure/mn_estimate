<?php

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ModuleManager,
	Bitrix\Main\Application,
	Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

class mn_estimate extends CModule {

	public $MODULE_ID,
		$MODULE_VERSION,
		$MODULE_VERSION_DATE,
		$MODULE_NAME,
		$MODULE_DESCRIPTION,

        $errors;

	function __construct() {

		$arModuleVersion = array();

		include_once (__DIR__ . '/version.php');

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {

			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_ID = 'mn.estimate';
		$this->MODULE_NAME = Loc::getMessage("MN_ESTIMATE_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("MN_ESTIMATE_INSTALL_DESCRIPTION");

		$this->PARTNER_NAME = Loc::getMessage("MN_ESTIMATE_INSTALL_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("MN_ESTIMATE_INSTALL_PARTNER_URI");
	}

	function InstallDB() {

        global $DB, $APPLICATION;
        $this->errors = false;

        if (!$DB->Query("SELECT 'x' FROM mn_estimate_items", true)) {

            $this->errors = $DB->RunSQLBatch(__DIR__ . '/db/mysql/install.sql');

            if ($this->errors === false) {
                $this->errors = $DB->RunSQLBatch(__DIR__.'/db/mysql/default.sql');
            }
        }

        if ($this->errors !== false) {

            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

		return true;
	}

	function UnInstallDB($arParams = array()) {

        global $DB, $APPLICATION;
        $this->errors = false;

        if (!array_key_exists("savedata", $arParams) || ($arParams["savedata"] !== "Y")) {

            $this->errors = $DB->RunSQLBatch(__DIR__ . '/db/mysql/uninstall.sql');
        }

        if ($this->errors !== false) {

            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        return true;
	}

	function InstallEvents() {

		return true;
	}

	function UnInstallEvents() {

		return true;
	}

	function InstallFiles() {

        CopyDirFiles(__DIR__ . "/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);

        return true;
	}

	function UnInstallFiles() {

		return true;
	}


	function DoInstall() {

		global $APPLICATION;

		$this->InstallDB();
		$this->InstallFiles();
		$this->InstallEvents();

		ModuleManager::registerModule($this->MODULE_ID);

		$APPLICATION->IncludeAdminFile(Loc::getMessage('MN_ESTIMATE_INSTALL_TITLE', ['#MODULE_NAME#' => $this->MODULE_ID]), __DIR__ . '/step.php');
	}

	function DoUninstall() {

		global $APPLICATION;

		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();

		if ((int) $request['step'] < 2) {
			$APPLICATION->IncludeAdminFile(Loc::getMessage('MN_ESTIMATE_UNINSTALL_TITLE', ['#MODULE_NAME#' => $this->MODULE_ID]), __DIR__ . '/unstep1.php');
		} elseif ((int) $request['step'] === 2) {

			$this->UnInstallEvents();
			$this->UnInstallFiles();

			if ($request['savedata'] != 'Y') {

				$this->UnInstallDB();
			}

			ModuleManager::unRegisterModule($this->MODULE_ID);

			$APPLICATION->IncludeAdminFile(Loc::getMessage('MN_ESTIMATE_UNINSTALL_TITLE', ['#MODULE_NAME#' => $this->MODULE_ID]), __DIR__ . '/unstep2.php');
		}
	}
}
