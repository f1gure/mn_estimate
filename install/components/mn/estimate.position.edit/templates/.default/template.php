<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Mn\Estimate\Orm\ServicesTable;
use Mn\Estimate\Orm\UnitsTable;

/** @var CBitrixComponentTemplate $this */

/** @var ErrorCollection $errors */
$errors = $arResult['ERRORS'];

foreach ($errors as $error) {
    /** @var Error $error */
    ShowError($error->getMessage());
}

$arServices = [];
$resServices = ServicesTable::getList(['select' => ['id', 'name']])->fetchAll();
if (!empty($resServices)) {
    foreach ($resServices as $arService) {
        $arServices[$arService['id']] = $arService['name'];
    }
}

$arUnits = [];
$resUnits = UnitsTable::getList(['select' => ['id', 'name'], 'order' => ['id' => 'asc']])->fetchAll();
if (!empty($resUnits)) {
    foreach ($resUnits as $arUnit) {
        $arUnits[$arUnit['id']] = $arUnit['name'];
    }
}

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'edit',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'N',
        'SHOW_SETTINGS' => 'N',
        'TITLE' => $arResult['TITLE'],
        'IS_NEW' => $arResult['IS_NEW'],
        'DATA' => $arResult['ITEM'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_TAB1_NAME'),
                'title' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_TAB1_TITLE'),
                'display' => false,
                'fields' => array(
                    array(
                        'id' => 'section_position',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_SECTION'),
                        'type' => 'section'
                    ),
                    array(
                        'id' => 'name',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_FIELD_NAME'),
                        'type' => 'text',
                        'value' => $arResult['ITEM']['name']
                    ),
                    array(
                        'id' => 'service_id',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_FIELD_SERVICE'),
                        'type' => 'list',
                        'items' => $arServices,
                        'value' => $arResult['ITEM']['service_id'],
                        'isTactile' => true
                    ),
                    array(
                        'id' => 'unit_id',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_FIELD_UNIT'),
                        'type' => 'list',
                        'items' => $arUnits,
                        'value' => $arResult['ITEM']['unit_id'],
                    ),
                    array(
                        'id' => 'price',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_FIELD_PRICE'),
                        'type' => 'int',
                        'value' => $arResult['ITEM']['price'],
                    ),
                    array(
                        'id' => 'price_ex',
                        'name' => Loc::getMessage('CT_MN_ESTIMATE_POSITION_EDIT_FIELD_PRICE_EX'),
                        'type' => 'text',
                        'value' => $arResult['ITEM']['price_ex'],
                    )
                )
            ),
        ),
        'BUTTONS' => array(
            'back_url' => $arResult['BACK_URL'],
            'standard_buttons' => true,
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
