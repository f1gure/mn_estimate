<?php
defined('B_PROLOG_INCLUDED') || die;

use Mn\Estimate\Orm\PositionsTable;
use Mn\Estimate\Orm\ServicesTable;
use Mn\Estimate\Orm\UnitsTable;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class CMnEstimatePositionEditComponent extends CBitrixComponent {
    const FORM_ID = 'MN_ESTIMATE_POSITION_EDIT';

    private $errors;

    public function __construct(CBitrixComponent $component = null) {

        parent::__construct($component);

        $this->errors = new ErrorCollection();

        CBitrixComponent::includeComponentClass('mn:estimate.position.list');
    }

    public function executeComponent() {

        global $APPLICATION;

        $title = Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_TITLE_DEFAULT');

        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_NO_MODULE_CRM'));
            return;
        }

        if (!Loader::includeModule('mn.estimate')) {
            ShowError(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_NO_MODULE'));
            return;
        }

        $arItem = array(
            'name' => '',
            'service_id' => 0,
            'unit_id' => 0,
            'price' => 0,
            'price_ex' => 0
        );

        if ((int) $this->arParams['POSITION_ID'] > 0) {
            $dbRes = PositionsTable::getById($this->arParams['POSITION_ID']);
            $arItem = $dbRes->fetch();

            if (empty($arItem)) {
                ShowError(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_ITEM_NOT_FOUND'));
                return;
            }
        }

        if (!empty($arItem['id'])) {
            $title = Loc::getMessage(
                'CC_MN_ESTIMATE_POSITION_EDIT_SHOW_TITLE',
                array(
                    '#ID#' => $arItem['id'],
                    '#NAME#' => $arItem['name']
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $savedId = $this->processSave($arItem);
            if ($savedId > 0) {
                LocalRedirect($this->getRedirectUrl($savedId));
            }

            $submitted = $this->getSubmittedPosition();
            $arItem = array_merge($arItem, $submitted);
        }

        $this->arResult = [
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => CMnEstimatePositionListComponent::GRID_ID,
            'IS_NEW' => empty($arItem['id']),
            'TITLE' => $title,
            'ITEM' => $arItem,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        ];

        $this->includeComponentTemplate();
    }

    private function processSave($initialItem) {

        $submittedItem = $this->getSubmittedPosition();

        $item = array_merge($initialItem, $submittedItem);

        $this->errors = self::validate($item);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        if (!empty($item['id'])) {
            $result = PositionsTable::update($item['id'], $item);
        } else {
            $result = PositionsTable::add($item);
        }

        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        return $result->isSuccess() ? $result->getId() : false;
    }

    private function getSubmittedPosition() {

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $submittedItem = [
            'name' => $request->get('name'),
            'service_id' => $request->get('service_id'),
            'unit_id' => $request->get('unit_id'),
            'price' => $request->get('price'),
            'price_ex' => $request->get('price_ex')
        ];

        return $submittedItem;
    }

    private static function validate($arItem) {

        $errors = new ErrorCollection();

        if (empty($arItem['name'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_NAME')));
        }

        if (empty($arItem['service_id'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_EMPTY_SERVICE')));
        } else {
            $res = ServicesTable::getById($arItem['service_id']);
            if ($res->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_UNKNOWN_SERVICE')));
            }
        }
        if (empty($arItem['unit_id'])) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_EMPTY_UNIT')));
        } else {
            $res = UnitsTable::getById($arItem['unit_id']);
            if ($res->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_UNKNOWN_UNIT')));
            }
        }

        if (empty($arItem['price']) || (int) $arItem['price'] <= 0) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_PRICE')));
        }
        if (empty($arItem['price_ex']) || (int) $arItem['price_ex'] <= 0) {
            $errors->setError(new Error(Loc::getMessage('CC_MN_ESTIMATE_POSITION_EDIT_VALIDATE_ERROR_PRICE_EX')));
        }

        return $errors;
    }

    private static function isFormSubmitted() {

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedId = null) {

        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedId) && $request->offsetExists('apply')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                ['POSITION_ID' => $savedId]
            );
        } elseif (!empty($savedId) && $request->offsetExists('saveAndAdd')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                ['POSITION_ID' => 0]
            );
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedId) && $request->offsetExists('saveAndView')) {
            return $this->arParams['SEF_FOLDER'];
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}
