<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponentTemplate $this */

$urlTemplates = [
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit']
];

$APPLICATION->IncludeComponent(
    'mn:estimate.item.edit',
    '',
    array(
        'ITEM_ID' => $arResult['VARIABLES']['ITEM_ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
