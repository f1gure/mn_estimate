<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */

$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';

$rows = array();
foreach ($arResult['ITEMS'] as $arItem) {

    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'],
        array('ITEM_ID' => $arItem['id'])
    );

    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($arItem['id']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;

    $rows[] = array(
        'id' => $arItem['id'],
        'actions' => array(),
        'data' => $arItem,
        'columns' => array(
            'id' => $arItem['id'],
            'company' => '<a href="/crm/company/show/' . $arItem['company_id'] . '/" target="_blank">' . $arItem['company_name'] . '</a>',
            'name' => $arItem['name'],
            'domain' => $arItem['domain'],
            'updated_at' => $arItem['updated_at'],
            'updated_by' => '<a href="/company/personal/user/' . $arItem['updated_user_id'] . '/" target="_blank">' . $arItem['updated_user_name'] . '</a>',
            'estimate_id' => (int) $arItem['estimate_id'] > 0 ? '<a href="' . CFile::GetPath($arItem['estimate_id']) . '" target="_blank">' . Loc::getMessage('CT_MN_ESTIMATE_ITEM_LIST_ESTIMATE_CAPTION') . '</a>' : '-',
            'estimate_ex_id' => (int) $arItem['estimate_ex_id'] > 0 ? '<a href="' . CFile::GetPath($arItem['estimate_ex_id']) . '" target="_blank">' . Loc::getMessage('CT_MN_ESTIMATE_ITEM_LIST_ESTIMATE_EX_CAPTION') . '</a>' : '-'
        )
    );
}

$snippet = new Snippet();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'EXTENSION' => array(
            'ID' => $gridManagerId,
            'CONFIG' => array(
                'ownerTypeName' => 'MN_ESTIMATE_ITEMS',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ),
            'MESSAGES' => array(
                'deletionDialogTitle' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_LIST_DELETE_DIALOG_TITLE'),
                'deletionDialogMessage' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_LIST_DELETE_DIALOG_MESSAGE'),
                'deletionDialogButtonTitle' => Loc::getMessage('CT_MN_ESTIMATE_ITEM_LIST_DELETE__DIALOG_BUTTON'),
            )
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
