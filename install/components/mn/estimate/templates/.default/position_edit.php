<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponentTemplate $this */

$urlTemplates = [
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['position_edit']
];

$APPLICATION->IncludeComponent(
    'mn:estimate.position.edit',
    '',
    array(
        'POSITION_ID' => $arResult['VARIABLES']['POSITION_ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['positions'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);
