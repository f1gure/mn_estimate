<?php

namespace Mn\Estimate\Orm;
use Bitrix\Main,
    Bitrix\Main\ORM,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class PositionsTable extends Main\Entity\DataManager {

    public static function getTableName() {

        return 'mn_estimate_positions';
    }

    public static function getMap() {

        return [
            'id' => new ORM\Fields\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_ID')
            ]),
            'service_id' => new ORM\Fields\IntegerField('service_id', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_SERVICE_ID')
            ]),
            'service' => new ORM\Fields\Relations\Reference(
                'service',
                ServicesTable::class,
                array('=this.service_id' => 'ref.id'),
                array('join_type' => 'left')
            ),
            'name' => new ORM\Fields\StringField('name', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_NAME')
            ]),
            'unit_id' => new ORM\Fields\IntegerField('unit_id', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_UNIT_ID')
            ]),
            'unit' => new ORM\Fields\Relations\Reference(
                'unit',
                UnitsTable::class,
                array('=this.unit_id' => 'ref.id'),
                array('join_type' => 'left')
            ),
            'price' => new ORM\Fields\IntegerField('price', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_PRICE')
            ]),
            'price_ex' => new ORM\Fields\IntegerField('price_ex', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_PRICE_EX')
            ]),
            'updated_by' => new ORM\Fields\IntegerField('updated_by', [
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_UPDATED_BY')
            ]),
            'updated_at_user' => new ORM\Fields\Relations\Reference(
                'updated_at_user',
                \Bitrix\Main\UserTable::class,
                array('=this.updated_by' => 'ref.ID'),
                array('join_type' => 'left')
            ),
            'updated_at' => new ORM\Fields\DatetimeField('updated_at', [
                'default_value' => function() {
                    return new Main\Type\DateTime();
                },
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITIONS_UPDATED_AT')
            ])
        ];
    }

    public static function validateName() {

        return [
            new ORM\Fields\Validators\LengthValidator(null, 255)
        ];
    }
}
