<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CC_MN_ESTIMATE_SEF_NOT_ENABLED'] = 'Режим ЧПУ должен быть включен.';
$MESS['CC_MN_ESTIMATE_SEF_BASE_EMPTY'] = 'Не указан каталог ЧПУ.';
$MESS['CC_MN_ESTIMATE_PERMISSION_DENIED'] = 'Нет доступа';
