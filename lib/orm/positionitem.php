<?php

namespace Mn\Estimate\Orm;
use Bitrix\Main,
    Bitrix\Main\ORM,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class PositionItemTable extends Main\Entity\DataManager {

    public static function getTableName() {

        return 'mn_estimate_position_item';
    }

    public static function getMap() {

        return [
            'id' => new ORM\Fields\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_ID')
            ]),

            'position_id' => (new ORM\Fields\IntegerField('position_id', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_POSITION_ID')
            ])),

            'position' => (new ORM\Fields\Relations\Reference('position', PositionsTable::class, ORM\Query\Join::on('this.position_id', 'ref.id')))->configureJoinType('inner'),

            'item_id' => (new ORM\Fields\IntegerField('item_id', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_ITEM_ID')
            ])),

            'item' => (new ORM\Fields\Relations\Reference('item', ItemsTable::class, ORM\Query\Join::on('this.item_id', 'ref.id')))->configureJoinType('inner'),

            'name' => new ORM\Fields\StringField('name', [
                'required' => true,
                'validation' => [__CLASS__, 'validateName'],
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_NAME')
            ]),

            'price' => new ORM\Fields\IntegerField('price', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_PRICE')
            ]),
            'count' => new ORM\Fields\IntegerField('count', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_COUNT')
            ]),
            'price_ex' => new ORM\Fields\IntegerField('price_ex', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_PRICE_EX')
            ]),
            'count_ex' => new ORM\Fields\IntegerField('count_ex', [
                'required' => true,
                'title' => Loc::getMessage('MN_ESTIMATE_ENTITY_POSITION_ITEM_COUNT_EX')
            ])
        ];
    }

    public static function validateName() {

        return [
            new ORM\Fields\Validators\LengthValidator(null, 255)
        ];
    }
}
