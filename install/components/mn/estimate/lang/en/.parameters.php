<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CP_MN_ESTIMATE_SEF_MODE_POSITIONS_URL_TEMPLATE'] = 'Шаблон пути к списку позиций услуг';
$MESS['CP_MN_ESTIMATE_SEF_MODE_POSITION_EDIT_URL_TEMPLATE'] = 'Шаблон пути к форме редактирования позиций услуг';
$MESS['CP_MN_ESTIMATE_SEF_MODE_EDIT_URL_TEMPLATE'] = 'Шаблон пути к форме редактирования сметы';
