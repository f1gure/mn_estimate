<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => GetMessage('CD_ESTIMATE_POSITION_LIST_NAME'),
	'DESCRIPTION' => GetMessage('CD_ESTIMATE_POSITION_LIST_DESCRIPTION'),
	'ICON' => '/images/list.gif',
	'SORT' => 10,
	'PATH' => array(
        'ID' => 'estimate',
        'NAME' => Loc::getMessage('CD_MN_ESTIMATE')
    )
);
